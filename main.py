import os
import subprocess
print(""" 
  _                    _____                      _ _             __   ___  
 | |                  / ____|                    (_| |           /_ | / _ \ 
 | |    _   _  __ _  | |     ___  _ __ ___  _ __  _| | ___ _ __   | || | | |
 | |   | | | |/ _` | | |    / _ \| '_ ` _ \| '_ \| | |/ _ | '__|  | || | | |
 | |___| |_| | (_| | | |___| (_) | | | | | | |_) | | |  __| |     | || |_| |
 |______\__,_|\__,_|  \_____\___/|_| |_| |_| .__/|_|_|\___|_|     |_(_\___/ 
                                           | |                              
                                           |_|                              
""")
print("PLEASE,BACK UP EVERY FILE.\n")

print("@Author: Ianito\n")
print("Put every file you want to encrypt on the \"resourcers\" folder. (Always back up first): \n")
path = "resources"
input("Press ENTER to continue!")
lua_compiler = os.getcwd() + "\\"+"luac_mta.exe "

def obsfuscate(scriptName, absolutePath):
    subprocess.Popen(lua_compiler+"-e2 -o "+absolutePath+" "+absolutePath)
        

def init_script(path):
    findSomething = False
    print("Compilating...")
    for root, dirs, files in os.walk(path, topdown=False):
        for name in files:
            if(name.endswith(".lua")):
                findSomething = True
                obsfuscate(name, root + "\\"+name)
    if(findSomething == False):
        print("Nothing to compile.")
        input("Press ENTER to finish.")
    


init_script(path)




